# preprocess.py
#
# Input: Feature vectors from the featureBuilder tool
# Output: Cross-validation and test sets for machine learning
#
# Distributes the generated feature vectors across cross-validation and test sets,
# keeping the original class distribution as best as possible.
# Also performs zero centering.

########## Settings #########
# number of classes (18 for euk, 6 for bac, 3 for arch)
numClasses = 18
# cross validation size
k_cv = 5
# kmer length used to generate the data
kmer_length = 3
# feature vector input directory (the binary output from the featureBuilder program)
raw_input_dir = "../featureBuilder/out/"
# output directory
output_dir = "./trainingData/"
#############################


import numpy as np
import os
import random

# sampleFromClasses
#   samples from <classSamples> according to <distribution>
#   classSamples: list of <numClasses> sublists containing identifiers of proteins in the respective class
#   distribution: list of <numClasses> integers indicating the amount of samples to draw from each class
#   returns list of drawn samples
def sampleFromClasses(classSamples, distribution):
    drawnSamples = []
    for i in range(numClasses):
        random.shuffle(classSamples[i])
        for j in range(distribution[i]):
            drawnSamples.append(classSamples[i].pop())
    return drawnSamples, classSamples

# load data and store identifiers per class
files = frozenset(os.listdir(raw_input_dir))
data = np.zeros((len(files), 20 ** kmer_length))
classMapping = {}
classCounter = np.zeros(numClasses, dtype=np.uint32)
classSamples = []
for i in range(numClasses):
    classSamples.append([])

for i,f in enumerate(files):
    data[i] = np.fromfile(raw_input_dir + f, dtype=np.int32)
    nameParts = f.split(".")
    identifier = nameParts[0]
    predClass = int(nameParts[1])
    classMapping[identifier] = predClass
    classCounter[predClass] += 1
    classSamples[predClass].append(identifier)

# compute average for zero-centering
avg = np.average(data, axis=0)

# compute distribution per set on original class counter
perClass, rem = np.divmod(classCounter, k_cv+1)
# we need at least one of each class in the test set
testDistribution = np.maximum(perClass, 1)
# compute distribution per set again without test set
classCounter -= testDistribution
perClass, rem = np.divmod(classCounter, k_cv)

# draw test samples
testSamples, classSamples = sampleFromClasses(classSamples, testDistribution)

# draw cross-validation datasets
# trainSamples dimensions:
# axis 0: cross-validation sets (k_cv)
# axis 1: samples per cv set (varying)
trainSamples = []
for i in range(k_cv):
    currentTrainSamples, classSamples = sampleFromClasses(classSamples, perClass)
    trainSamples.append(currentTrainSamples)

# place remaining samples into bins in round-robin fashion
currentBin = 0
for i in range(numClasses):
    toDistribute = rem[i]
    while toDistribute > 0:
        trainSamples[currentBin].append(classSamples[i].pop())
        currentBin += 1
        currentBin %= k_cv
        toDistribute -= 1

# classSamples has to be empty by now, maybe add a sanity check here..

if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# normalize and save cross-validation sets seperately as numpy arrays
for i in range(k_cv):
    setData = np.zeros((len(trainSamples[i]), 20 ** kmer_length))
    labels = np.zeros(len(trainSamples[i]))
    
    for j, ident in enumerate(trainSamples[i]):
        fileName = raw_input_dir + ident + "." + str(classMapping[ident]) + ".bin"
        setData[j] = np.fromfile(fileName, dtype=np.int32)
        labels[j] = classMapping[ident]
    setData -= avg

    print("CV set {}".format(i+1))
    print("Data size: {}".format(setData.shape))
    print("Label size: {}".format(labels.shape))

    fileName = "trainDataset_" + str(i)
    np.savez(output_dir + fileName, setData, labels)

# normalize and save test data
setData = np.zeros((len(testSamples), 20 ** kmer_length))
labels = np.zeros(len(testSamples))
    
for j, ident in enumerate(testSamples):
    fileName = raw_input_dir + ident + "." + str(classMapping[ident]) + ".bin"
    setData[j] = np.fromfile(fileName, dtype=np.int32)
    labels[j] = classMapping[ident]
setData -= avg

print("Test set")
print("Data size: {}".format(setData.shape))
print("Label size: {}".format(labels.shape))

fileName = "testDataset"
np.savez(output_dir + fileName, setData, labels)

print("\nTest class vector to double-check distribution (all classes represented?):")
print(labels)
