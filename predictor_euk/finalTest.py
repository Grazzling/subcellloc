# finalTest.py
#
# Input: Neural network model
# Output: Performance measures
#
# Reports performance of the generated model on the final test set.

######## Settings ########
# number of classes (18 for euk, 6 for bac, 3 for arch)
numClasses = 18
# cross validation sets
k_cv = 5
# kmer length
kmer_length = 3
# batch size
batch_size = 1016
# training data
train_data = "./trainingData_s5.5/"
# model
model = "./best_oversample.model"
# enable cuda
cuda = True

# final hyperparameters
hidden_units = 350
##########################


import numpy as np
import torch
from torch.autograd import Variable
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
import matplotlib.pyplot as plt
from SubCellLocDataset import *
from SubCellLocNet import *

net = SubCellLocNet(kmer_length=kmer_length, hidden=hidden_units, numClasses=numClasses)
if cuda:
    net.cuda()

net.load_state_dict(torch.load(model))

testLoader = DataLoader(SubCellLocDataset(kmer_length=kmer_length, k_cv=k_cv, trainingDir=train_data, train=False), batch_size=batch_size, shuffle=False)

# evaluate on test set
correct = 0
total = 0
TP = np.zeros(numClasses)
FP = np.zeros(numClasses)
FN = np.zeros(numClasses)
classTotals = np.zeros(numClasses)
pred_all = np.array([], dtype=np.int32)
labels_all = np.array([], dtype=np.int32)
for data in testLoader:
    inputs, labels = data
    inputs = Variable(inputs)
    inputs = inputs.float()
    labels = labels.long()
    if cuda:
        inputs = inputs.cuda()
        labels = labels.cuda()

    outputs = net(inputs)
    _, predicted = torch.max(outputs.data, 1)
    pred_cpu = predicted.cpu().numpy()
    labels_cpu = labels.cpu().numpy()

    pred_all = np.concatenate((pred_all, pred_cpu))
    labels_all = np.concatenate((labels_all, labels_cpu))

    total += labels.size(0)
    correct += (pred_cpu == labels_cpu).sum()

    for i in range(labels.size(0)):
        trueClass = labels_cpu[i]
        predClass = pred_cpu[i]
        classTotals[trueClass] += 1
        if trueClass == predClass:
            TP[trueClass] += 1
        else:
            FP[predClass] += 1
            FN[trueClass] += 1

# bootstrapping
bootstrapping_iters = 1000
bootstrapping_subsampling = 0.6
Q_list = []
acc_list = np.zeros((numClasses, bootstrapping_iters))
cov_list = np.zeros((numClasses, bootstrapping_iters))
subsampleCount = int(total * bootstrapping_subsampling)
for i in range(bootstrapping_iters):
    idx = np.random.permutation(total)[:subsampleCount]

    TP_subsample = np.zeros(numClasses)
    FP_subsample = np.zeros(numClasses)
    FN_subsample = np.zeros(numClasses)

    for j in range(idx.shape[0]):
        index = idx[j]
        trueClass = labels_all[index]
        predClass = pred_all[index]
        if trueClass == predClass:
            TP_subsample[trueClass] += 1
        else:
            FP_subsample[predClass] += 1
            FN_subsample[trueClass] += 1
    Q_list.append(TP_subsample.sum() / subsampleCount)
    for c in range(numClasses):
        acc = 0.0
        cov = 0.0
        if TP_subsample[c]+FP_subsample[c] > 0.0:
            acc = 100*TP_subsample[c] / (TP_subsample[c]+FP_subsample[c])
        if TP_subsample[c]+FN_subsample[c] > 0.0:
            cov = 100*TP_subsample[c] / (TP_subsample[c]+FN_subsample[c])
        acc_list[c][i] = acc
        cov_list[c][i] = cov

print("Correct: {}/{} Q{}: {:.2f} (+-{:.2f})".format(correct, total, numClasses, correct/total, np.std(Q_list)))

for i in range(numClasses):
    acc = 0.0
    cov = 0.0
    if TP[i]+FP[i] > 0.0:
        acc = 100*TP[i] / (TP[i]+FP[i])
    if TP[i]+FN[i] > 0.0:
        cov = 100*TP[i] / (TP[i]+FN[i])
    print("Class %d --- TP/FP/FN/Total: %d/%d/%d/%d acc: %.01f%% cov: %.01f%% acc_stddev: %.01f%% cov_stddev: %.01f%%" % (int(i+1), int(TP[i]), int(FP[i]), int(FN[i]), classTotals[i], acc, cov, np.std(acc_list[i]), np.std(cov_list[i])))
