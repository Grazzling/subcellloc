# hyperOptimize.py
#
# Input: Cross-validation and test sets for machine learning
# Output: No direct output for another tool, but generates log files of parameter combinations
#         and savesa a graph plot of the best combination found over all runs
#
# Randomly samples hyperparameters for learning rate, regularization strength and layer sizes
# and checks average accuracy by k-fold cross-validation.
# Meant to be run multiple times, increasing the amount of epochs and narrowing the intervals of hyperparameters.

######## Settings ########
# number of classes (18 for euk, 6 for bac, 3 for arch)
numClasses = 18
# cross validation sets
k_cv = 5
# kmer length
kmer_length = 3
# oversampling minority classes
oversample = True
# amount of optimization sample runs
optim_iters = 10
# amount of epochs to perform for each sample run
epochs = 40
# batch size
batch_size = 1016
# training data
train_data = "./trainingData_s5.5/"
# enable cuda
cuda = True
# verbose output
verbose = True

# Also look at the hyperparameter intervals at the start of the main loop!

##########################


import numpy as np
import torch
from torch.autograd import Variable
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
import matplotlib.pyplot as plt
import random
from sys import maxsize
from SubCellLocDataset import *
from SubCellLocNet import *

excludes = []
mappings = []

# examples for usage of excludes and mappings to test specific classes against each other
# mem-Nmem
#excludes = []
#mappings = ((2, 1), (3, 1), (4, 1), (5, 0), (6, 0), (7, 0), (8, 0), (9, 0), (10, 0), (11, 0), (12, 1), (13, 1), (14, 1), (15, 1), (16, 1), (17, 1))
#numClasses = 2
# Nmem -> sec-Nsec
#excludes = (0, 5, 6, 7, 8, 9, 10, 11)
#mappings = ((2, 1), (3, 0), (4, 0), (12, 1), (13, 1), (14, 1), (15, 1), (16, 0), (17, 1))
#numClasses = 2
# mem -> sec-Nsec
#excludes = (1, 2, 3, 4, 12, 13, 14, 15, 16, 17)
#mappings = ((5, 1), (6, 0), (7, 0), (8, 1), (9, 1), (10, 1), (11, 1))
#numClasses = 2

trainLoaders = []
for i in range(k_cv):
    trainLoaders.append(DataLoader(SubCellLocCVDataset(kmer_length=kmer_length, k_cv=k_cv, valNr=i, trainingDir=train_data, oversample=oversample, excludes=excludes, mappings=mappings), batch_size=batch_size, shuffle=True))
testLoaders = []
for i in range(k_cv):
    testLoaders.append(DataLoader(SubCellLocCVDataset(kmer_length=kmer_length, k_cv=k_cv, valNr=i, train=False, trainingDir=train_data, excludes=excludes, mappings=mappings), batch_size=1, shuffle=False))

# defines one run of hyperparameter sampling
def trainIteration(k_cv, epochs, batch_size, hidden_units, lr):

    losses = np.zeros((k_cv, epochs))
    validationAccuracies = np.zeros((k_cv, epochs))
    trainAccuracies = np.zeros((k_cv, epochs))

    seed = random.randint(0, maxsize)

    # do cross validation with k_cv datasets and compute average of best validation accuracy
    for cv_iter in range(k_cv):
        if verbose:
            print("Starting CV iteration {}".format(cv_iter))
        torch.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)

        net = SubCellLocNet(kmer_length=kmer_length, hidden=hidden_units, numClasses=numClasses)
        if cuda:
            net.cuda()

        criterion = nn.CrossEntropyLoss()
        optimizer = optim.Adam(net.parameters(), lr=lr)

        for epoch in range(epochs):

            # test on validation set (before training to also capture random performance at the first epoch)
            correct = 0
            total = 0
            for data in testLoaders[cv_iter]:
                inputs, labels = data
                inputs = Variable(inputs)
                inputs = inputs.float()
                labels = labels.long()
                if cuda:
                    inputs = inputs.cuda()
                    labels = labels.cuda()

                outputs = net(inputs)
                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum()
            valAcc = 100 * correct / total
            validationAccuracies[cv_iter][epoch] = valAcc

            # test on train set
            correct = 0
            total = 0
            for data in trainLoaders[cv_iter]:
                inputs, labels = data
                inputs = Variable(inputs)
                inputs = inputs.float()
                labels = labels.long()
                if cuda:
                    inputs = inputs.cuda()
                    labels = labels.cuda()

                outputs = net(inputs)
                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum()
            trainAcc = 100 * correct / total
            trainAccuracies[cv_iter][epoch] = trainAcc

            # training step
            total = 0
            running_loss = 0.0
            for i, data in enumerate(trainLoaders[cv_iter], 0):
                inputs, labels = data
                inputs, labels = Variable(inputs), Variable(labels)
                inputs = inputs.float()
                labels = labels.long()
                if cuda:
                    inputs = inputs.cuda()
                    labels = labels.cuda()

                optimizer.zero_grad()

                outputs = net(inputs)
                loss = criterion(outputs, labels)
                loss.backward()
                optimizer.step()

                running_loss += loss.data[0]
                total += labels.size(0)

            avg_loss = running_loss / total
            losses[cv_iter][epoch] = avg_loss

    losses = np.average(losses, axis=0)
    validationAccuracies = np.average(validationAccuracies, axis=0)
    trainAccuracies = np.average(trainAccuracies, axis=0)

    bestValidationAccuracy = validationAccuracies.max()

    return seed, bestValidationAccuracy, losses, validationAccuracies, trainAccuracies

def log(line):
    f = open("log.txt", "a+")
    f.write(line)
    f.close()

# main loop
bestAccuracy = 0.0
for iter in range(optim_iters):
    # optimize the range of the following hyperparameters
    lr = 10 ** np.random.uniform(-4, -3)
    hidden_units = np.random.uniform(10, 1000)
    hidden_units = int(hidden_units)

    print("{}/{} Trying lr: {} hiddenUnits: {}".format(iter, optim_iters, lr, hidden_units,))
    seed, curAccuracy, loss, valAcc, trainAcc = trainIteration(k_cv, epochs, batch_size, hidden_units, lr)
    log(str(lr) + " " + str(hidden_units) + " " + str(seed) + " " + str(curAccuracy) + "\n")
    print("    bestAcc: {}".format(curAccuracy))

    if curAccuracy > bestAccuracy:
        bestAccuracy = curAccuracy
        print("********Found new best parameter set: lr {}  hidden {}".format(lr, hidden_units))

        f, axarr = plt.subplots(2, sharex=True)
        axarr[0].title.set_text('Training results: lr {}\nhidden {}\nseed {}'.format(lr, hidden_units, seed))
        axarr[0].plot(loss, color="b")
        axarr[0].set_xlabel('epoch')
        axarr[0].set_ylabel('loss')
        axarr[0].set_ylim(ymin=0)
        axarr[0].yaxis.set_label_position("right")
        axarr[0].yaxis.tick_right()
        axarr[1].plot(valAcc, color="r")
        axarr[1].plot(trainAcc, color="g")
        axarr[1].set_xlabel('epoch')
        axarr[1].set_ylabel('accuracy')
        axarr[1].set_ylim(ymax=100,ymin=0)
        axarr[1].yaxis.set_label_position("right")
        axarr[1].yaxis.tick_right()
        plotName = "best.png"
        plt.savefig(plotName)
        
