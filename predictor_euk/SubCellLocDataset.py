from torch.utils.data import Dataset
import numpy as np

# cross-validation dataset class (used in hyperOptimize.py)
class SubCellLocCVDataset(Dataset):
    def __init__(self, kmer_length, k_cv, valNr, trainingDir, train=True, oversample=False, excludes=[], mappings=[]):
        """
        :param k_cv: total number of cross validation sets
        :param valNr: index of the validation set
        :param train: boolean switch for training/test mode
        :param trainingDir: directory containing training data
        """
        self.k_cv = k_cv
        self.valNr = valNr
        self.train = train
        self.trainingDir = trainingDir
        self.totalLength = 0

        self.data = np.array([]).reshape(0, 20 ** kmer_length)
        self.labels = np.array([])

        for i in range(k_cv):
            if (i==valNr and self.train) or (i!=valNr and not self.train):
                continue
            fileName = trainingDir + "trainDataset_" + str(i) + ".npz"
            npzfile = np.load(fileName)
            self.data = np.concatenate((self.data, npzfile['arr_0']))
            self.labels = np.concatenate((self.labels, npzfile['arr_1']))

        for excl in excludes:
            idx_list = self.labels != excl
            self.data = self.data[idx_list]
            self.labels = self.labels[idx_list]
        for first,second in mappings:
            self.labels[self.labels == first] = second

        if oversample and self.train:
            # count classes
            numClasses = int(self.labels.max()) + 1
            classCounts = np.zeros(numClasses, dtype=np.uint32)
            for i in range(self.labels.shape[0]):
                classCounts[int(self.labels[i])] += 1
            #print(classCounts)

            classMax = classCounts.max()

            # iterate over classes and duplicate samples
            origLength = self.labels.shape[0]
            for classLabel in range(numClasses):
                if classCounts[classLabel] == 0:
                    continue

                classData = np.empty((classCounts[classLabel], 20**kmer_length))
                curPos = 0
                for i in range(origLength):
                    if int(self.labels[i]) == classLabel:
                        classData[curPos] = self.data[i]
                classLabels = np.empty(classCounts[classLabel])
                classLabels.fill(classLabel)

                # how often do we need to fill in
                fillAmount = (classMax // classCounts[classLabel]) -  1
                remainder = classMax % classCounts[classLabel]

                fillRemainder = fillAmount
                fillExp = 2
                numDouble = 0
                while fillExp < fillAmount:
                    numDouble += 1
                    fillRemainder = fillAmount - fillExp
                    fillExp *= 2

                print("Class {}, {}/{}, fillAmount: {} remainder: {} numDouble: {} fillRemainder: {}".format(classLabel, classCounts[classLabel], classMax, fillAmount, remainder, numDouble, fillRemainder))

                fillData = np.array([]).reshape(0, 20 ** kmer_length)
                fillLabels = np.array([])
                for i in range(numDouble):
                    if i==0:
                        fillData = np.concatenate((fillData, classData))
                        fillLabels = np.concatenate((fillLabels, classLabels))
                    fillData = np.concatenate((fillData, fillData))
                    fillLabels = np.concatenate((fillLabels, fillLabels))
                for i in range(fillRemainder):
                    fillData = np.concatenate((fillData, classData))
                    fillLabels = np.concatenate((fillLabels, classLabels))
                fillData = np.concatenate((fillData, classData[:remainder]))
                fillLabels = np.concatenate((fillLabels, classLabels[:remainder]))
                self.data = np.concatenate((self.data, fillData))
                self.labels = np.concatenate((self.labels, fillLabels))

            classCounts = np.zeros(numClasses, dtype=np.uint32)
            for i in range(self.labels.shape[0]):
                classCounts[int(self.labels[i])] += 1
            print(classCounts)

        self.totalLength = self.labels.shape[0]

    def __len__(self):
        return self.totalLength

    def __getitem__(self, idx):
        dataRow = self.data[idx]
        label = self.labels[idx]
        return dataRow, label

# full dataset for final training and evaluation (used in fullTrain.py and finalTest.py)
class SubCellLocDataset(Dataset):
    def __init__(self, kmer_length, k_cv, trainingDir, train=True, oversample=False, excludes=[], mappings=[]):
        """
        :param k_cv: total number of cross validation sets
        :param valNr: index of the validation set
        :param train: boolean switch for training/test mode
        :param trainingDir: directory containing training data
        """
        self.k_cv = k_cv
        self.train = train
        self.trainingDir = trainingDir
        self.totalLength = 0

        self.data = np.array([]).reshape(0, 20 ** kmer_length)
        self.labels = np.array([])

        if train:
            for i in range(k_cv):
                fileName = trainingDir + "trainDataset_" + str(i) + ".npz"
                npzfile = np.load(fileName)
                self.data = np.concatenate((self.data, npzfile['arr_0']))
                self.labels = np.concatenate((self.labels, npzfile['arr_1']))
        else:
            fileName = trainingDir + "testDataset.npz"
            npzfile = np.load(fileName)
            self.data = np.concatenate((self.data, npzfile['arr_0']))
            self.labels = np.concatenate((self.labels, npzfile['arr_1']))

        for excl in excludes:
            idx_list = self.labels != excl
            self.data = self.data[idx_list]
            self.labels = self.labels[idx_list]
        for first,second in mappings:
            self.labels[self.labels == first] = second

        if oversample and self.train:
            # count classes
            numClasses = int(self.labels.max()) + 1
            classCounts = np.zeros(numClasses, dtype=np.uint32)
            for i in range(self.labels.shape[0]):
                classCounts[int(self.labels[i])] += 1
            #print(classCounts)

            classMax = classCounts.max()

            # iterate over classes and duplicate samples
            origLength = self.labels.shape[0]
            for classLabel in range(numClasses):
                if classCounts[classLabel] == 0:
                    continue

                classData = np.empty((classCounts[classLabel], 20**kmer_length))
                curPos = 0
                for i in range(origLength):
                    if int(self.labels[i]) == classLabel:
                        classData[curPos] = self.data[i]
                classLabels = np.empty(classCounts[classLabel])
                classLabels.fill(classLabel)

                # how often do we need to fill in
                fillAmount = (classMax // classCounts[classLabel]) -  1
                remainder = classMax % classCounts[classLabel]

                fillRemainder = fillAmount
                fillExp = 2
                numDouble = 0
                while fillExp < fillAmount:
                    numDouble += 1
                    fillRemainder = fillAmount - fillExp
                    fillExp *= 2

                print("Class {}, {}/{}, fillAmount: {} remainder: {} numDouble: {} fillRemainder: {}".format(classLabel, classCounts[classLabel], classMax, fillAmount, remainder, numDouble, fillRemainder))

                fillData = np.array([]).reshape(0, 20 ** kmer_length)
                fillLabels = np.array([])
                for i in range(numDouble):
                    if i==0:
                        fillData = np.concatenate((fillData, classData))
                        fillLabels = np.concatenate((fillLabels, classLabels))
                    fillData = np.concatenate((fillData, fillData))
                    fillLabels = np.concatenate((fillLabels, fillLabels))
                for i in range(fillRemainder):
                    fillData = np.concatenate((fillData, classData))
                    fillLabels = np.concatenate((fillLabels, classLabels))
                fillData = np.concatenate((fillData, classData[:remainder]))
                fillLabels = np.concatenate((fillLabels, classLabels[:remainder]))
                self.data = np.concatenate((self.data, fillData))
                self.labels = np.concatenate((self.labels, fillLabels))

            classCounts = np.zeros(numClasses, dtype=np.uint32)
            for i in range(self.labels.shape[0]):
                classCounts[int(self.labels[i])] += 1
            print(classCounts)

        self.totalLength = self.labels.shape[0]

    def __len__(self):
        return self.totalLength

    def __getitem__(self, idx):
        dataRow = self.data[idx]
        label = self.labels[idx]
        return dataRow, label
