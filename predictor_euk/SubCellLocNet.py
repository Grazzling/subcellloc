import torch.nn as nn
import torch.nn.functional as F

class SubCellLocNet(nn.Module):
    def __init__(self, kmer_length, hidden, numClasses):
        super(SubCellLocNet, self).__init__()

        self.fc1 = nn.Linear(20 ** kmer_length, hidden)
        self.fc2 = nn.Linear(hidden, numClasses)
        #self.fc2 = nn.Linear(20 ** kmer_length, numClasses)

    def forward(self, x):
        x = F.leaky_relu(self.fc1(x))
        x = self.fc2(x)
        return x
