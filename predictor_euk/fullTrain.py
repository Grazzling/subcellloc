# fullTrain.py
#
# Input: Cross-validation and test sets for machine learning
# Output: Neural network model
#
# Trains a final model based on good parameters found during hyperparameter optimization.

######## Settings ########
# number of classes (18 for euk, 6 for bac, 3 for arch)
numClasses = 18
# cross validation sets
k_cv = 5
# kmer length
kmer_length = 3
# oversample minority classes
oversample = True
# amount of epochs
epochs = 40
# batch size
batch_size = 1016
# training data
train_data = "./trainingData_s5.5/"
# model output
model_out = "./best_oversample.model"
# enable cuda
cuda = True

# final hyperparameters
lr = 0.0007190235786106256
hidden_units = 350
seed = 630387499155723891
##########################


import numpy as np
import torch
from torch.autograd import Variable
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
import matplotlib.pyplot as plt
from SubCellLocDataset import *
from SubCellLocNet import *

torch.manual_seed(seed)
torch.cuda.manual_seed_all(seed)

net = SubCellLocNet(kmer_length=kmer_length, hidden=hidden_units, numClasses=numClasses)
if cuda:
    net.cuda()

trainLoader = DataLoader(SubCellLocDataset(kmer_length=kmer_length, k_cv=k_cv, trainingDir=train_data, oversample=oversample), batch_size=batch_size, shuffle=True)

criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(net.parameters(), lr=lr)

for epoch in range(epochs):
    for i, data in enumerate(trainLoader, 0):
        inputs, labels = data
        inputs, labels = Variable(inputs), Variable(labels)
        inputs = inputs.float()
        labels = labels.long()
        if cuda:
            inputs = inputs.cuda()
            labels = labels.cuda()

        optimizer.zero_grad()

        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

torch.save(net.state_dict(), model_out)
