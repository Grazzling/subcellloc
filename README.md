## SubCellLoc

Prediction toolchain for sub-cellular localization of proteins.

The toolchain consists of two main parts:

1. Feature builder: Computes kmer feature vectors from protein profiles

2. Predictor: Uses a neural network to classify the localization of proteins based on their feature vectors

---

** Feature builder **

The feature builder is a C++ program that uses an optimized tree method to compute kmer feature vectors efficiently.
The program expects the FASTA protein lists 'eukaryota.1682.fa', 'bacteria.479.fa' and 'archaea.59.fa' to exist in the same directory as the executable and the PSI-BLAST profiles of those proteins in the sub-directory 'profiles'.
The output (kmer feature vectors) are written in the directory 'out', following the scheme <identifier>.<class>.bin as binary files containing the 20^k counts as integers.

The feature builder expects three command line parameters

1. Type: Either 'euk', 'bac' or 'arch', indicating the type of organism.

2. kmer length: The length of the kmers (e.g. 3)

3. Threshold: The sigma threshold (e.g. 7.5)

Example usage: > ./featureBuilder euk 3 7.5

To compile the feature builder you need to have boost headers installed (at least boost string), no external libraries are necessary.

---

** Predictor **

Prediction is based on neural networks, implemented with pytorch. To train bigger networks in reasonable time, you need a NVIDIA GPU and CUDA, although training on the CPU is possible for testing purposes.

The toolchain for the prediction consists of four seperate scripts. All of those scripts contain a settings section at the top, where most of the important parameters of the script can be modified.

1. preprocess.py:
During preprocessing, the feature vectors are distributed into cross-validation and test sets.

2. hyperOptimize.py:
The hyperparameters (learning rate, hidden layer size) need to be optimized by running hyperOptimize.py multiple times, usually each time with a higher number of epochs and narrowed intervals for the hyperparameters.

3. fullTrain.py:
After good hyperparameters have been found, the final model is being trained with fullTrain.py, which uses all cross-validation sets at the same time.

4. finalTest.py:
The generated model is being evaluated on the test set, reporting performance measures on all classes, such as accuracy and coverage.

The file 'SubCellLocNet.py' defines the network architecture used in all scripts, while 'SubCellLocDataset.py' implements a dataset loader in two variants: One for the cross-validation sets and a slightly modified one for the final training and evaluation.

---

** Trying your own predictor: A small tutorial **

- Feature builder

1. Compile the feature builder, put the required files (FASTA lists and profiles) in the directory of the executable and run it with the parameters that you need, e.g. ./featureBuilder bac 3 6

2. Duplicate the existing 'predictor_euk' directory and rename it, e.g. to 'predictor_bac'. We're going to modify the scripts in there for the new prediction job.

- Preprocessing

4. Take a look at preprocess.py and adjust the settings, especially the number of classes for the type of organism, the kmer length and maybe the amount of cross-validation sets (if you try bacteria, maybe use less than 5 because of the smaller dataset).

5. Run preprocess.py, it should generate your training data in the directory 'trainingData'.

- Optimization

6. Take a look at hyperOptimize.py and adjust the parameters again. Make sure that the number of classes, the amount of cross-validation sets and kmer length match the earlier settings.

7. Run hyperOptimize.py several times, each time with a higher number of epochs. Between each run, look at the log file and the generated plots. Adjust the hyperparameters to narrow them down to good values.

- Final evaluation

8. When you're happy with your hyperparameters, edit fullTrain.py, adjust the settings again and enter your hyperparameter values. Run the script to generate the final model.

9. Adjust the settings of finalTest.py as usual and put in your hyperparameters again (at least the layer size, the rest is already incorporated into the model). Run the script and evaluate your results.