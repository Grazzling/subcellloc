//
// Created by konstantin on 11/26/17.
//

#ifndef FEATUREBUILDER_PROFILEFEATURECOMPOSER_H
#define FEATUREBUILDER_PROFILEFEATURECOMPOSER_H

#include <vector>
#include <iostream>

struct posEntry {
    int pos;
    float score;
};

struct treeNode {
    int depth;
    char AA;
    std::vector<posEntry> posList;
    treeNode* parent;
    treeNode* childs[20] = {0};
};

class ProfileKmerTree {
public:
    ProfileKmerTree(std::string profileFile, char k, float sigma);
    void printKMers();
    void writeOutputVector(std::string outFile, bool binary=false);
    bool isSane();

private:
    void printExpand(treeNode* node, int depth);
    void outputExpand(std::ofstream& outStream, treeNode* node, int depth, bool binary);

    void loadProfile(std::string profileFile);
    void buildTree();
    void expand(treeNode* node, int depth);


    std::vector<float*> profileMat;
    treeNode* roots[20];
    const int k;
    const float sigma;
    const float MAX_VALUE = 1000.0;
    char AAMap[20];
    bool insane;
};


#endif //FEATUREBUILDER_PROFILEFEATURECOMPOSER_H
