cmake_minimum_required(VERSION 3.7)
project(featureBuilder)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp ProfileKmerTree.cpp ProfileKmerTree.h)
add_executable(featureBuilder ${SOURCE_FILES})