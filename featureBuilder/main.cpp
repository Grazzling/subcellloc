#include "ProfileKmerTree.h"

#include <fstream>
#include <boost/algorithm/string.hpp>
#include <sys/types.h>
#include <sys/stat.h>
#include <map>
#include <set>

// Archaea
int mapClassToNumberArch(std::string classString) {
    if (!classString.compare("cellmembrane"))
        return 0;
    if (!classString.compare("cytoplasm"))
        return 1;
    if (!classString.compare("secreted"))
        return 2;
}

// Bacteria
int mapClassToNumberBac(std::string classString) {
    if (!classString.compare("cytoplasm"))
        return 0;
    if (!classString.compare("fimbrium"))
        return 1;
    if (!classString.compare("inner_membrane"))
        return 2;
    if (!classString.compare("outer_membrane"))
        return 3;
    if (!classString.compare("periplasm"))
        return 4;
    if (!classString.compare("secreted"))
        return 5;
}

// Eukaryotes
int mapClassToNumberEuka(std::string classString) {
    if (!classString.compare("cellmembrane"))
        return 0;
    if (!classString.compare("chloroplast"))
        return 1;
    if (!classString.compare("cytoplasm"))
        return 2;
    if (!classString.compare("er"))
        return 3;
    if (!classString.compare("golgi"))
        return 4;
    if (!classString.compare("memchloroplast"))
        return 5;
    if (!classString.compare("memer"))
        return 6;
    if (!classString.compare("memgolgi"))
        return 7;
    if (!classString.compare("memmitochondria"))
        return 8;
    if (!classString.compare("memnucleus"))
        return 9;
    if (!classString.compare("memperoxisome"))
        return 10;
    if (!classString.compare("memvacuole"))
        return 11;
    if (!classString.compare("mitochondria"))
        return 12;
    if (!classString.compare("nucleus"))
        return 13;
    if (!classString.compare("peroxisome"))
        return 14;
    if (!classString.compare("plastid"))
        return 15;
    if (!classString.compare("secreted"))
        return 16;
    if (!classString.compare("vacuole"))
        return 17;
}

void printUsage(std::string programName) {
    std::cout << "Protein sub-cellular localization prediction" << std::endl;
    std::cout << "Feature vector computation" << std::endl;
    std::cout << "Usage: " << programName << " <euk|bac|arch> <k-mer length> <sigma threshold>" << std::endl;
    std::cout << "Example: " << programName << " euk 3 5.0" << std::endl;
}

int main(int argc, char** argv) {

    // Parameter handling
    if (argc != 4) {
        printUsage(argv[0]);
        return -1;
    }
    std::string type(argv[1]);
    int k = atoi(argv[2]);
    float sigma = atof(argv[3]);

    std::string outDirectory = "./out/";


    // Set input file and type number mapping to selected organism type
    std::string fastaFile;
    int numClasses;
    int (*mapClassToNumber)(std::string) = nullptr;
    if (type.compare("euk") == 0) {
        fastaFile = "eukaryota.1682.fa";
        numClasses = 18;
        mapClassToNumber = mapClassToNumberEuka;
    }
    else if (type.compare("bac") == 0) {
        fastaFile = "bacteria.479.fa";
        numClasses = 6;
        mapClassToNumber = mapClassToNumberBac;
    }
    else if (type.compare("arch") == 0) {
        fastaFile = "archaea.59.fa";
        numClasses = 3;
        mapClassToNumber = mapClassToNumberArch;
    }
    else {
        printUsage(argv[0]);
        return -1;
    }


    // Load FASTA file and read in map from identifier to localization
    std::ifstream fastaStream(fastaFile);
    if (!fastaStream.good()) {
        std::cout << "Error: Couldn't open FASTA file " << fastaFile << std::endl;
        return -1;
    }
    std::map<std::string, std::string> fastaDict;
    std::set<std::string> classes;

    std::string line;
    while (std::getline(fastaStream, line))
    {
        std::vector<std::string> results;
        boost::trim_if(line, boost::is_any_of("\t "));
        boost::split(results, line, boost::is_any_of("\t "), boost::token_compress_on);

        if (results.size() == 2) {
            fastaDict[results[0].substr(1)] = results[1];
            classes.emplace(results[1]);
        }
    }

    fastaStream.close();


    // Check for output directory
    struct stat info;
    if (stat(outDirectory.c_str(), &info) != 0) {
        std::cout << "Warning: Output directory doesn't exist, creating it now.." << std::endl;
        const int dir_err = mkdir("out", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        if (dir_err == -1) {
            std::cout << "Error: Couldn't create output directory" << std::endl;
            return -1;
        }
    }
    else if (!(info.st_mode & S_IFDIR)) {
        std::cout << "Error: " << outDirectory << " is not a directory" << std::endl;
        return -1;
    }


    // Process profiles
    int classCounts[numClasses] = {0};
    int insaneCounts[numClasses] = {0};
    int processedCount = 0;
    int insaneCount = 0;
    for (auto& fastaPair : fastaDict) {
        std::cout << "Processing " << fastaPair.first << " " << fastaPair.second << std::endl;

        std::string profileFileName = "profiles/" + fastaPair.first + ".blastPsiMat";
        std::ifstream profileFile(profileFileName);
        if (!profileFile.good()) {
            std::cout << "Warning: Couldn't open profile " << profileFileName << std::endl;
            continue;
        }

        ProfileKmerTree tree("profiles/" + fastaPair.first + ".blastPsiMat", k, sigma);
        if (!tree.isSane()) {
            ++insaneCount;
            insaneCounts[mapClassToNumber(fastaPair.second)]++;
            continue;
        }

        std::string outName = outDirectory + fastaPair.first + "." + std::to_string(mapClassToNumber(fastaPair.second)) + ".bin";
        tree.writeOutputVector(outName, true);

        classCounts[mapClassToNumber(fastaPair.second)]++;

        ++processedCount;
    }


    // Print statistics
    std::cout << std::endl << "Class summary:" << std::endl;
    for (auto& entry : classes) {
        std::cout << entry << " " << classCounts[mapClassToNumber(entry)] << "(insane: " << insaneCounts[mapClassToNumber(entry)] << ")" << std::endl;
    }

    std::cout << std::endl;
    std::cout << "Total profiles: " << std::to_string(fastaDict.size()) << std::endl;
    std::cout << "Insane profiles: " << std::to_string(insaneCount) << std::endl;
    std::cout << "Processed profiles: " << std::to_string(processedCount) << std::endl;

    return 0;
}
