//
// Created by konstantin on 11/26/17.
//

#include "ProfileKmerTree.h"
#include <boost/algorithm/string.hpp>
#include <fstream>
#include <math.h>

ProfileKmerTree::ProfileKmerTree(std::string profileFile, char k, float sigma)
    : k(k), sigma(sigma), insane(false) {
    AAMap[0] = 'A';
    AAMap[1] = 'R';
    AAMap[2] = 'N';
    AAMap[3] = 'D';
    AAMap[4] = 'C';
    AAMap[5] = 'Q';
    AAMap[6] = 'E';
    AAMap[7] = 'G';
    AAMap[8] = 'H';
    AAMap[9] = 'I';
    AAMap[10] = 'L';
    AAMap[11] = 'K';
    AAMap[12] = 'M';
    AAMap[13] = 'F';
    AAMap[14] = 'P';
    AAMap[15] = 'S';
    AAMap[16] = 'T';
    AAMap[17] = 'W';
    AAMap[18] = 'Y';
    AAMap[19] = 'V';

    loadProfile(profileFile);

    if (!insane)
        buildTree();
}

void ProfileKmerTree::printKMers() {
    for (int i=0 ; i<20 ; ++i) {
        printExpand(roots[i], 1);
    }
}

void ProfileKmerTree::writeOutputVector(std::string outFile, bool binary) {
    std::ios_base::openmode flags = std::ios::out;
    if (binary)
        flags |= std::ios::binary;
    std::ofstream outStream(outFile, flags);

    for (int i=0 ; i<20 ; ++i) {
        outputExpand(outStream, roots[i], 1, binary);
    }

    outStream.close();
}

void ProfileKmerTree::printExpand(treeNode* node, int depth) {
    if (depth==k) {
        std::vector<char> AA;
        AA.push_back(node->AA);
        treeNode* parent = node->parent;
        while (parent != nullptr) {
            AA.push_back(parent->AA);
            parent = parent->parent;
        }

        printf("Got k-mer contribution: ");
        std::string aaStr;
        for (char c : AA)
            aaStr.push_back(AAMap[c]);
        std::reverse(aaStr.begin(), aaStr.end());
        printf("%s %d\n", aaStr.c_str(), node->posList.size());

        return;
    }

    for (int i=0 ; i<20 ; ++i) {
        if (node->childs[i])
            printExpand(node->childs[i], depth+1);
    }
}

void ProfileKmerTree::outputExpand(std::ofstream& outStream, treeNode* node, int depth, bool binary) {
    if (depth==k) {
        if (binary) {
            int size = node->posList.size();
            outStream.write(reinterpret_cast<const char *>(&size), 4);
        }
        else
            outStream << std::to_string(node->posList.size()) + " ";
        return;
    }

    for (int i=0 ; i<20 ; ++i) {
        if (node->childs[i])
            outputExpand(outStream, node->childs[i], depth+1, binary);
        else {
            int missedElements = pow(20, k-depth-1);
            for (int j=0 ; j<missedElements ; ++j) {
                if (binary) {
                    int zero = 0;
                    outStream.write(reinterpret_cast<const char *>(&zero), 4);
                }
                else
                    outStream << "0 ";
            }
        }
    }
}

void ProfileKmerTree::loadProfile(std::string profileFile) {
    std::ifstream profileStream(profileFile);

    int idx = 0;
    int saneLines = 0;
    std::string line;
    while (std::getline(profileStream, line))
    {
        std::vector<std::string> results;
        boost::trim_if(line, boost::is_any_of("\t "));
        boost::split(results, line, boost::is_any_of("\t "), boost::token_compress_on);

        if (results.size() > 42) {
            float* profileLine = new float[20];
            int sanitySum = 0;
            int sanityEpsilon = 15;
            for (int i=0 ; i<20 ; ++i) {
                int percentage = std::atoi(results[i+22].c_str());
                if (percentage == 0)
                    profileLine[i] = MAX_VALUE;
                else
                    profileLine[i] = -log(percentage/100.0);
                sanitySum += percentage;
            }
            if (sanitySum < 100-sanityEpsilon || sanitySum > 100+sanityEpsilon)
                ;//printf("SANITY CHECK FAILURE: Amino acid frequency at pos %d doesn't add up! Sum was %d\n", idx, sanitySum);
            else
                ++saneLines;
            profileMat.push_back(profileLine);
            ++idx;
        }
    }

    float saneFraction = float(saneLines) / idx;
    if (saneFraction < 0.5) {
        printf("SANITY SUMMARY: Skipping %s, sanity score is %f\n", profileFile.c_str(), saneFraction);
        this->insane = true;
    }

    profileStream.close();
}

void ProfileKmerTree::buildTree() {
    // initialize and expand root nodes
    for (int i=0 ; i<20 ; ++i) {
        roots[i] = new treeNode;
        roots[i]->parent = nullptr;
        roots[i]->AA = i;
        roots[i]->depth = 1;

        // iterate over all positions
        for (int pos=0 ; pos<profileMat.size() ; ++pos) {
            if (profileMat[pos][i] < sigma) {
                posEntry pe;
                pe.pos = pos;
                pe.score = profileMat[pos][i];
                roots[i]->posList.push_back(pe);
            }
        }

        expand(roots[i], 2);
    }
}

void ProfileKmerTree::expand(treeNode* node, int depth) {
    // abort if there are no entries below sigma
    if (node->posList.size() == 0)
        return;

    for (int i=0 ; i<20 ; ++i) {
        node->childs[i] = new treeNode;
        node->childs[i]->parent = node;
        node->childs[i]->AA = i;

        for (posEntry pe : node->posList) {
            if (pe.pos + 1 >= profileMat.size())
                continue;
            if (pe.score + profileMat[pe.pos+1][i] < sigma) {
                posEntry newpe;
                newpe.pos = pe.pos + 1;
                newpe.score = pe.score + profileMat[pe.pos+1][i];
                node->childs[i]->posList.push_back(newpe);
            }
        }

        if (node->childs[i]->posList.size() == 0) {
            delete node->childs[i];
            node->childs[i] = nullptr;
        }
        else if (depth < k)
            expand(node->childs[i], depth+1);
    }
}

bool ProfileKmerTree::isSane() {
    return !this->insane;
}